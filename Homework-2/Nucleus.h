#pragma once
#include <iostream>

class Gene {
	public:
		/**
		 * @brief initialises the variables
		 * @param start: start index
		 * @param end: end index
		 * @param on_complementary_dna_strand: some bool
		*/
		void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

		//getters and setters
		int get_start() const;
		void set_start(const unsigned start);

		int get_end() const;
		void set_end(const unsigned end);

		//other methods
		bool is_on_complementary_dna_strand() const;
		void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
		
	private:
		unsigned int _start;
		unsigned int _end;
		bool _on_complementary_dna_strand;
};

class Nucleus {
	public:
		/**
		 * @brief initializes Nucleus with given dna sequence
		 * @param dna_sequence some dna sequence
		*/
		void init(const std::string dna_sequence);

		/**
		 * @brief returns dna transcript
		 * @param gene: gene to process
		 * @return string with dna transcript
		*/
		std::string get_RNA_transcript(const Gene& gene) const;

		/**
		 * @brief returns reversed dna strand
		 * @return string with reversed dna strand
		*/
		std::string get_reversed_DNA_strand() const;

		/**
		 * @brief returns frequency of given codon in dna strand
		 * @param codon string with some codon
		 * @return frequency of codon in
		*/
		unsigned int get_num_of_codon_appearances(const std::string& codon) const;
	
	private:
		std::string _DNA_strand;
		std::string _complementary_DNA_strand;

};