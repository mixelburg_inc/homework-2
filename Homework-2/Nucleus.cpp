#include "Nucleus.h"

int Gene::get_start() const{
	return static_cast<int>(this->_start);
}

void Gene::set_start(const unsigned start){
	this->_start = start;
}

int Gene::get_end() const{
	return static_cast<int>(this->_end);
}

void Gene::set_end(const unsigned end){
	this->_end = end;
}

bool Gene::is_on_complementary_dna_strand() const{
	return this->_on_complementary_dna_strand;
}

void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand){
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand){

	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}


void Nucleus::init(const std::string dna_sequence) {

	this->_DNA_strand = dna_sequence;

	this->_complementary_DNA_strand = "";
	for (const auto& nucl : dna_sequence) {
		switch (nucl) {
		case 'G':
			this->_complementary_DNA_strand.push_back('C');
			break;
		case 'C':
			this->_complementary_DNA_strand.push_back('G');
			break;
		case 'T':
			this->_complementary_DNA_strand.push_back('A');
			break;
		case 'A':
			this->_complementary_DNA_strand.push_back('T');
			break;
		default:
			std::cerr << "invalid string passed to init method in class Nucleus";
			exit(1);
		}
	}
}


std::string Nucleus::get_RNA_transcript(const Gene& gene) const {

	std::string used_strand;
	if (gene.is_on_complementary_dna_strand()){
		used_strand = this->_complementary_DNA_strand;
	}
	else {
		used_strand = this->_DNA_strand;
	}

	std::string transcript = "";
	for (auto i = gene.get_start(); i <= gene.get_end(); i++){
		if (used_strand[i] == 'T'){
			transcript.push_back('U');
		}
		else{
			transcript.push_back(used_strand[i]);
		}
	}
	
	return transcript;
}


std::string Nucleus::get_reversed_DNA_strand() const {

	std::string rev = std::string(this->_DNA_strand.rbegin(), this->_DNA_strand.rend());
	return rev;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const{

	int count = 0;
	for (size_t offset = this->_DNA_strand.find(codon); offset != std::string::npos;
		offset = this->_DNA_strand.find(codon, offset + codon.length()))
	{
		++count;
	}
	return count;
}