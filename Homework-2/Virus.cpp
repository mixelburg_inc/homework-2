#include "Virus.h"

void Virus::init(const std::string RNA_sequence) {
	this->_RNA_sequence = RNA_sequence;
}

void Virus::infect_cell(Cell& cell) const {
	//get access to the private variables
	Nucleus* nucleus = (Nucleus*)&cell;

	//get reversed DNA strand
	std::string reversed_DNA_strand = nucleus->get_reversed_DNA_strand();

	//reverse it back
	std::string normal_DNA_strand = std::string(reversed_DNA_strand.rbegin(), reversed_DNA_strand.rend());

	//insert the virus
	normal_DNA_strand.insert(static_cast<int>(normal_DNA_strand.length() / 2), this->_RNA_sequence);

	//reinitialize the cell
	nucleus->init(normal_DNA_strand);
}