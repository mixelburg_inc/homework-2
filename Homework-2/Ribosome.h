#pragma once
#include "Protein.h"


class Ribosome {
	public:
		/**
		 * @brief returns a new Proteins based on RNA transctipt
		 * @param RNA_transcript some transcript
		 * @return new protein
		*/
		Protein* create_protein(std::string& RNA_transcript) const;
};

class Mitochondrion {
	public:
		/**
		 * @brief initializes new Mitochondrion
		*/
		void init();

		/**
		 * @brief inserts new receptor
		 * @param protein new protein
		*/
		void insert_glucose_receptor(const Protein& protein);

		/**
		 * @brief sets glucose level
		 * @param glocuse_units level number
		*/
		void set_glucose(const unsigned int glocuse_units);

		/**
		 * @brief returns true in Mitochondrion can produce atp
		 * @return bool value
		*/
		bool produceATP() const;
	
	private:
		unsigned int _glocuse_level;
		bool _has_glocuse_receptor;
};
