#pragma once
#include <iostream>
#include "Cell.h"

class Virus {
	public:
		/**
		 * @brief initializes the Virus with given RNA sequence
		 * @param RNA_sequence given RNA sequence
		*/
		void init(const std::string RNA_sequence);
	
		/**
		 * @brief infects the given Cell using Viruses RNA sequence
		 *		  (inserts the RNA sequence in the middle of cell's sequence)
		 * @param cell given Cell
		*/
		void infect_cell(Cell& cell) const;
	private:
		std::string _RNA_sequence;
};
