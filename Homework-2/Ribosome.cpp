#include "Ribosome.h"
#include <iostream>


Protein* Ribosome::create_protein(std::string& RNA_transcript) const {
	Protein* new_protein = new Protein;
	new_protein->init();

	while (RNA_transcript.length() >= 3) {
		std::string first_3 = RNA_transcript.substr(0, 3);
		RNA_transcript.erase(0, 3);

		const AminoAcid new_amino_acid = get_amino_acid(first_3);
		if (new_amino_acid == UNKNOWN) {
			new_protein->clear();
			break;
		}
		else {
			new_protein->add(new_amino_acid);
		}
	}

	return new_protein;
}

void Mitochondrion::init() {
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void  Mitochondrion::insert_glucose_receptor(const Protein& protein) {
	AminoAcidNode* first = protein.get_first();

	if (protein.get_nth(0) == ALANINE && 
		protein.get_nth(1) == LEUCINE &&
		protein.get_nth(2) == GLYCINE &&
		protein.get_nth(3) == HISTIDINE &&
		protein.get_nth(4) == LEUCINE &&
		protein.get_nth(5) == PHENYLALANINE &&
		protein.get_nth(6) == AMINO_CHAIN_END) {

		this->_has_glocuse_receptor = true;
	}
}


void Mitochondrion::set_glucose(const unsigned int glocuse_units) {
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const {
	return this->_glocuse_level >= 50 && this->_has_glocuse_receptor;
}
