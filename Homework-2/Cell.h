#pragma once
#include "Ribosome.h"
#include "Nucleus.h"

class Cell {
	public:
		/**
		 * @brief initializes the Cell
		 * @param dna_sequence some sequence
		 * @param glucose_receptor_gene some Gene
		*/
		void init(const std::string dna_sequence, const Gene glucose_receptor_gene);

		/**
		 * @brief checks if the Cell can produce energy and returns true if if can of false if not
		 * @return bool
		*/
		bool get_ATP();

	private:
		Nucleus _nucleus;
		Ribosome _ribosome;
		Mitochondrion _mitochondrion;
		Gene _glocus_receptor_gene;
		unsigned int _atp_utints;
};
